(ns cannapi.core
  (:require [clojure.set :as set]
            [cheshire.core :as json]
            [clj-http.client :as http]
            [clojure.string :as str]))

(def menu-url "https://menu-belltown.haveaheartcc.com/")

(def start-string "var menu_start_data       = [")
(def end-string "];\n")

(defn get-menu []
  (-> menu-url http/get :body))

(defn extract-menu-data [menu]
  (let [start (+ 28 (str/index-of menu start-string))
        end   (inc (str/index-of menu end-string start))]
    (subs menu start end)))

(def csrf-regex #".*name=\"csrf-token\" content=\"(.*)\".*")
(defn extract-csrf [menu]
  (let [xf (comp (mapcat (partial re-matches csrf-regex))
                 (filter identity))
        [_ csrf] (sequence xf (str/split-lines menu))]
    csrf))

(defn decode-menu-data [menu-data]
  (json/parse-string menu-data true))

(defn load-menu []
  (-> (get-menu)
      extract-menu-data
      decode-menu-data))

(def find-carts-xf
  (comp (filter #(= "Concentrate" (:product_type %)))
        (filter #(re-matches #"(?i).*cartridge.*" (:name %)))
        (filter #(nil? (re-matches #"(?i).*pax pod.*" (:name %))))))

(defn find-carts [menu]
    (sequence find-carts-xf menu))

(defn filter-by-key [menu k & opts]
  (filter
   (fn [menu-item]
     (let [val (get menu-item k)]
       (contains? (set opts) val)))
   menu))


(def dank-xf
  (mapcat                                                         ;; flatMap for clojure seqs
   (fn [{:keys [:thc_percentage :menu_row_prices] :as menu-item}] ;; mapping over menu items
     (map
      (fn [{:keys [:product_weight :price] :as price-item}]       ;; mapping over menu_row_prices
        (let [g (/ (Double. product_weight) 100)
              t (Double. thc_percentage)
              p (Double. price)]
          (assoc price-item                                       ;; calculate thc_gram_per_dollar
                 :thc_gram_per_dollar                             ;; and add it to price-item
                 (/ (* t g)
                    p)
                 :thc_percentage
                 thc_percentage)))
      menu_row_prices))))

(defn better-find-dank-carts
  ([menu] (better-find-dank-carts menu nil))
  ([menu num]
   (let [find-dank-carts-xf (comp find-carts-xf dank-xf)       ;; filter for carts first
         dank-carts         (sequence find-dank-carts-xf menu) ;; process for dankness
         sorted-carts       (sort-by :thc_gram_per_dollar dank-carts)]
     (if num
       (take-last num sorted-carts)
       sorted-carts))))

(defn format-carts [carts]
  (map #(select-keys % [:original_name
                        :thc_gram_per_dollar
                        :product_weight
                        :thc_percentage
                        :price])
       carts))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (-> (load-menu)
      (better-find-dank-carts)
      format-carts
      clojure.pprint/pprint))
